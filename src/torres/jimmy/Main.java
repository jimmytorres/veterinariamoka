package torres.jimmy;

import torres.jimmy.bl.entities.Cita;
import torres.jimmy.bl.entities.Mascota;
import torres.jimmy.bl.entities.Reservacion;
import torres.jimmy.bl.entities.Usuario;

import torres.jimmy.tl.Controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public static void main(String args[]) throws java.io.IOException
    {
        Controller controlador = new Controller();
        controlador.iniciar();
    }

  /*
     static Cita[] citas = new Cita[100];
    static Reservacion[] reservaciones = new Reservacion[100];
    static Usuario[] usuarios = new Usuario[100];
    static Mascota[] mascotas = new Mascota[100];

    static int posicionMascotas = 0;
    static int posicionCitas = 0;
    static int posicionReservaciones = 0;
    static int posicionUsuarios = 0;

    public static void main(String args[]) throws java.io.IOException
    {

        int opcion = -1;
        do {
            opcion = mostrarMenu();
            procesarOpcion(opcion);
        } while (opcion != 0);

    }

    static int mostrarMenu() throws java.io.IOException
    {
        int opcion;
        opcion = 0;
        out.println("1. Registrar Mascota");
        out.println("2. Listar Mascotas");
        out.println("3. Registrar Usuario");
        out.println("4. Listar Usuarios");
        out.println("5. Registrar Reservacion");
        out.println("6. Listar Reservaciones");
        //out.println("7. Registrar Cita");
        out.println("8. Listar Citas");
        out.println("0. Salir del sistema");
        out.println("Digite la opción");
        opcion = Integer.parseInt(in.readLine());
        return opcion;
    }

    static void procesarOpcion(int pOpcion) throws java.io.IOException
    {
        switch (pOpcion)
        {
            case 1:
                registrarMascota();
                break;
            case 2:
                listarMascotas();
                break;
            case 3:
                registrarUsuario();
                break;
            case 4:
                listarUsuarios();
                break;
            case 5:
                registrarReservacion();
                break;
            case 6:
                listarReservaciones();
                break;
            case 7:
                registrarCita();
                break;
            case 8:
                listarCitas();
                break;
            case 0:
                out.println("Fin del programa!");
                break;
            default:
                out.println("Opcion inválida");
        }
    }

    static void registrarMascota() throws java.io.IOException{
        String nombre;
        String foto;
        String observaciones;
        int ranking;
        String nombreDueno;
        String cedula;
        String telefono;
        String direccion;

        Mascota tmpMascota;

        // - - - Nombre de la mascota
        boolean nombresDiferentes = true;
        String nombreComparar = "";
        do{
            if (nombresDiferentes) {
                out.println("Digite el nombre de la mascota: ");
                nombre = in.readLine();
            } else {
                out.println("Digite de nuevo nombre de la mascota: ");
                nombre = in.readLine();
            }
            nombresDiferentes = true;
            for (int i = 0; i < posicionMascotas; i++) {
                    nombreComparar = mascotas[i].getNombreMascota();
                    if (nombreComparar.equalsIgnoreCase(nombre)) {
                        nombresDiferentes = false;
                        out.println("Hay una mascota con el mismo nombre. Intentar de nuevo.");
                        break;
                    }
                }
        }while( !nombresDiferentes );

        out.println("Ingrese la foto de la mascota: "); foto = in.readLine();
        out.println("Digite el nombre del dueño: "); nombreDueno = in.readLine();
        out.println("Digite la cedula del dueño: "); cedula = in.readLine();
        out.println("Digite el telefono del dueño "); telefono = in.readLine();
        out.println("Digite la dirección del dueño "); direccion = in.readLine();
        out.println("Digite los cuidados especiales de la mascota: "); observaciones = in.readLine();

        boolean rankingValido;
        //String rankingInput;
        do {
            rankingValido = true;
            out.println("Digite el ranking (de 1 a 3): ");
            ranking = Integer.parseInt(in.readLine());

            if( !((ranking == 1) || (ranking == 2) || (ranking == 3))){
                rankingValido = false;
                System.out.println("No es un numero entre 1 y 3.");
            }

        } while(!rankingValido);

        tmpMascota = new Mascota(nombre, foto, nombreDueno, cedula, telefono, direccion, observaciones, ranking);
        mascotas[posicionMascotas] = tmpMascota;
        aumentarPosicionMascotas();

    }

    static void registrarCita() throws java.io.IOException {

        String nombreMascota;
        String fechaCita;
        String horaCita;
        String observaciones;

        out.println("Digite el nombre de la mascota: "); nombreMascota = in.readLine();
        out.println("Digite la fecha de la cita: "); fechaCita = in.readLine();
        out.println("Digite la hora de la cita: "); horaCita = in.readLine();
        out.println("Digite las observaciones de la cita: "); observaciones = in.readLine();

        Cita tmpCita = new Cita(nombreMascota, fechaCita, horaCita, observaciones);
        citas[posicionCitas] = tmpCita;
        aumentarPosicionCitas();

    }

    static void registrarReservacion() throws java.io.IOException {

        String nombreMascota;
        String fechaEntrada;
        String fechaSalida;
        Reservacion tmpReservacion;

        out.println("Digite el nombre de la mascota: "); nombreMascota = in.readLine();
        out.println("Digite la fecha de entrada: "); fechaEntrada = in.readLine();
        out.println("Digite la fecha de salida: "); fechaSalida = in.readLine();

        tmpReservacion = new Reservacion(nombreMascota, fechaEntrada, fechaSalida);
        reservaciones[posicionReservaciones] = tmpReservacion;
        aumentarPosicionReservaciones();
    }

    static void registrarUsuario() throws java.io.IOException {

        String nombreUsuario;
        String cedula;
        String telefono;
        String direccion;
        int estado; //activo/inactivo
        int rol; // doctor/secretaria
        Usuario tmpUsuario;

        out.println("Digite el nombre del usuario: "); nombreUsuario = in.readLine();

        // - - - Cedula del usuario
        boolean cedulasDiferentes = true;
        String cedulaComparar = "";
        do{
            if (cedulasDiferentes) {
                out.println("Digite la cedula: ");
                cedula = in.readLine();
            } else {
                out.println("Digite de nuevo la cedula: ");
                cedula = in.readLine();
            }
            cedulasDiferentes = true;
            for (int i = 0; i < posicionUsuarios; i++) {
                cedulaComparar = usuarios[i].getCedula();
                if (cedulaComparar.equalsIgnoreCase(cedula)) {
                    cedulasDiferentes = false;
                    out.println("Hay un usuario con la misma cedula. Intentar de nuevo.");
                    break;
                }
            }
        }while( !cedulasDiferentes );
        //------------

        out.println("Digite el numero de telefono: "); telefono = in.readLine();
        out.println("Digite la direccion: "); direccion = in.readLine();

        do{
            out.println("Digite el estado (1 si es activo, 2 si es inactivo): "); estado = Integer.parseInt(in.readLine());
            if((estado != 1) && (estado != 2)){ out.println("El numero que ingreso no es 1 ni 2. Intentar de nuevo..."); }
        }while ( (estado != 1) && (estado != 2) );

        do{
            out.println("Digite el rol: (1 si es doctor, 2 si es secretaria): "); rol = Integer.parseInt(in.readLine());
            if((rol != 1) && (rol != 2)){ out.println("El numero que ingreso no es 1 ni 2. Intentar de nuevo..."); }
        }while ( (rol != 1) && (rol != 2) );


        tmpUsuario = new Usuario(nombreUsuario, cedula, telefono, direccion, estado, rol);
        usuarios[posicionUsuarios] = tmpUsuario;
        aumentarPosicionUsuarios();
    }

    static void aumentarPosicionMascotas(){ posicionMascotas++; }
    static void listarMascotas(){
        out.println("Lista de mascotas en la Veterinaria MOKA");
        for(int i = 0;i<posicionMascotas;i++){
            out.println(mascotas[i].toString());
        }
    }

    static void aumentarPosicionCitas(){ posicionCitas++; }
    static void listarCitas(){
        out.println("Lista de citas en la Veterinaria MOKA");
        for(int i = 0;i<posicionCitas;i++){
            out.println(citas[i].toString());
        }
    }

    static void aumentarPosicionReservaciones(){ posicionReservaciones++; }
    static void listarReservaciones(){
        out.println("Lista de reservaciones en la Veterinaria MOKA");
        for(int i = 0;i<posicionReservaciones;i++){
            out.println(reservaciones[i].toString());
        }
    }

    static void aumentarPosicionUsuarios(){ posicionUsuarios++; }
    static void listarUsuarios(){
        out.println("Lista de usuarios en la Veterinaria MOKA");
        for(int i = 0;i<posicionUsuarios;i++){
            out.println(usuarios[i].toString());
        }
    }

    */

}


