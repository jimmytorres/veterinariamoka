package torres.jimmy.bl;

import torres.jimmy.bl.entities.*;
import torres.jimmy.dl.Datos;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;


public class Gestor {
    private Datos db;

    public Gestor() { db = new Datos(); }

    /**
     *
     * @param nombre
     * @param fotoMascota
     * @param nombreDueno
     * @param cedula
     * @param telefono
     * @param direccion
     * @param observaciones
     * @param ranking
     */
    public void registrarMascota(String nombre, String fotoMascota, String observaciones, int ranking,
                                 String nombreDueno, String cedula, String telefono, String direccion){
        Mascota tmpMascota = new Mascota(nombre, fotoMascota, observaciones, ranking, nombreDueno,
                                        cedula, telefono, direccion);
        db.registrarMascota(tmpMascota);
    }
    public ArrayList<Mascota> listarMascotas() { return db.getMascotas(); }

    /**
     *
     * @param nombreMascota
     * @param fechaCita
     * @param horaCita
     * @param observaciones
     */
    public void registrarCita(String nombreMascota, LocalDate fechaCita, LocalTime horaCita, String observaciones){
        Cita tmpCita = new Cita(nombreMascota, fechaCita, horaCita, observaciones);
        db.registrarCita(tmpCita);
    }
    public ArrayList<Cita> listarCitas() { return db.getCitas(); }

    /**
     *
     * @param nombreMascota
     * @param fechaEntrada
     * @param fechaSalida
     */
    public void registrarReservacion(String nombreMascota, LocalDate fechaEntrada, LocalDate fechaSalida){
        Reservacion tmpReservacion = new Reservacion(nombreMascota, fechaEntrada, fechaSalida);
        db.registrarReservacion(tmpReservacion);
    }
    public ArrayList<Reservacion> listarReservaciones() { return db.getReservaciones(); }

    /**
     *
     * @param nombreUsuario
     * @param cedula
     * @param telefono
     * @param direccion
     * @param estado
     * @param rol
     */
    public void registrarUsuario(String nombreUsuario, String cedula, String telefono,
                                 String direccion, int estado, int rol){
        Usuario tmpUsuario = new Usuario(nombreUsuario, cedula, telefono, direccion, estado, rol);
        db.registrarUsuario(tmpUsuario);
    }
    public ArrayList<Usuario> listarUsuarios() { return db.getUsuarios(); }

}
