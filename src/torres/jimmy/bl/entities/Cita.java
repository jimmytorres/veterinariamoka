package torres.jimmy.bl.entities;

import java.time.LocalDate;
import java.time.LocalTime;

public class Cita {

    private String nombreMascota;
    private LocalDate fechaCita;
    private LocalTime horaCita;
    private String observaciones;

    public Cita(){

    }

    public Cita(String nombreMascota, LocalDate fechaCita, LocalTime horaCita, String observaciones){
        this.nombreMascota = nombreMascota;
        this.fechaCita = fechaCita;
        this.horaCita = horaCita;
        this.observaciones = observaciones;
    }

    public String getNombreMascota() {
        return nombreMascota; }
    public void setNombreMascota(String nombreMascota) {
        this.nombreMascota = nombreMascota; }

    public LocalDate getFechaCita() {
        return fechaCita; }
    public void setFechaCita(LocalDate fechaCita) {
        this.fechaCita = fechaCita; }

    public LocalTime getHoraCita() {
        return horaCita; }
    public void setHoraCita(LocalTime horaCita) {
        this.horaCita = horaCita; }

    public String getObservaciones() {
        return observaciones; }
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones; }

    @Override
    public String toString() {
        return "Cita{" +
                "nombreMascota='" + nombreMascota + '\'' +
                ", fechaCita='" + fechaCita + '\'' +
                ", horaCita='" + horaCita + '\'' +
                ", observaciones='" + observaciones + '\'' +
                '}';
    }


}
