package torres.jimmy.bl.entities;

public class Mascota {

    private String nombreMascota;
    private String fotoMascota;
    private String observaciones; // cuidados particulares que se deba tener la mascota
    private int ranking; // 1,2 o 3 estrellas, puesta por el doctor que lo atendio
    //-----------Dueño
    private String nombreDueno;
    private String cedula;
    private String telefono;
    private String direccion;


    public Mascota(){

    }

    public Mascota(String nombre, String fotoMascota, String observaciones, int ranking,
                   String nombreDueno, String cedula, String telefono, String direccion){
        this.nombreMascota = nombre;
        this.fotoMascota = fotoMascota;
        this.observaciones = observaciones;
        this.ranking = ranking;
        this.nombreDueno = nombreDueno;
        this.cedula = cedula;
        this.telefono = telefono;
        this.direccion = direccion;

    }


    public String getNombreMascota() {
        return nombreMascota;
    }
    public void setNombreMascota(String nombreMascota) {
        this.nombreMascota = nombreMascota;
    }

    public String getFotoMascota() {
        return fotoMascota;
    }
    public void setFotoMascota(String fotoMascota) {
        this.fotoMascota = fotoMascota;
    }

    public String getNombreDueno() {
        return nombreDueno;
    }
    public void setNombreDueno(String nombreDueno) {
        this.nombreDueno = nombreDueno;
    }

    public String getCedula() {
        return cedula;
    }
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getObservaciones() {
        return observaciones;
    }
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getRanking() {
        return ranking;
    }
    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    @Override
    public String toString() {
        return "Mascota{" +
                "nombre de mascota='" + nombreMascota + '\'' +
                ", foto de mascota='" + fotoMascota + '\'' +
                ", observaciones='" + observaciones + '\'' +
                ", ranking= " + ranking + " estrella(s)" + "\'" +
                ", nombre de dueño='" + nombreDueno + '\'' +
                ", cedula='" + cedula + '\'' +
                ", telefono='" + telefono + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }

}

/*
    //citas
    private String nombreMascota;
    private String fechaCita;
    private String horaCita;
    private String observaciones; // para que es la cita

    //reservaciones
    private String nombreMascota;
    private String fechaEntrada;
    private String fechaSalida;

    //registro usuarios
    private String nombreUsuario;
    private String cedula;
    private String telefono;
    private String direccion;
    private boolean estado; //activo/inactivo
    private boolean rol; // doctor/secretaria

    private String nombreMascota;
    private String fotoMascota;
    private String observaciones;
    private int ranking; // 1,2 o 3 estrellas

    private String nombreDueño;
    private String cedula;
    private String telefono;
    private String direccion;

    private String fotoMascota;

*/