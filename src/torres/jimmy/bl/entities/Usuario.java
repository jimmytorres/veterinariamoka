package torres.jimmy.bl.entities;

public class Usuario {

    private String nombreUsuario;
    private String cedula;
    private String telefono;
    private String direccion;
    private int estado; // (1 si es activo, 2 si es inactivo)
    private int rol; // (1 si es doctor, 2 si es secretaria)

    public Usuario(){

    }

    public Usuario(String nombreUsuario, String cedula, String telefono,
                    String direccion, int estado, int rol){
        this.nombreUsuario = nombreUsuario;
        this.cedula = cedula;
        this.telefono = telefono;
        this.direccion = direccion;
        this.estado = estado;
        this.rol = rol;
    }

    public String getNombreUsuario() {
        return nombreUsuario; }
    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario; }

    public String getCedula() {
        return cedula; }
    public void setCedula(String cedula) {
        this.cedula = cedula; }

    public String getTelefono() {
        return telefono; }
    public void setTelefono(String telefono) {
        this.telefono = telefono; }

    public String getDireccion() {
        return direccion; }
    public void setDireccion(String direccion) {
        this.direccion = direccion; }

    public int isEstado() {
        return estado; }
    public void setEstado(int estado) {
        this.estado = estado; }

    public int isRol() {
        return rol; }
    public void setRol(int rol) {
        this.rol = rol; }


    @Override
    public String toString() {
        String stringEstado;
        if(this.estado == 1){ stringEstado = "Activo"; } else { stringEstado = "Inactivo";}

        String stringRol;
        if(this.rol == 1){ stringRol = "Doctor"; } else { stringRol = "Secretaria";}

        return "Usuario{" +
                "nombreUsuario='" + nombreUsuario + '\'' +
                ", cedula='" + cedula + '\'' +
                ", telefono='" + telefono + '\'' +
                ", direccion='" + direccion + '\'' +
                ", estado= " + stringEstado +
                ", rol= " + stringRol +
                '}';
    }


}
