package torres.jimmy.dl;

import torres.jimmy.bl.entities.Cita;
import torres.jimmy.bl.entities.Mascota;
import torres.jimmy.bl.entities.Reservacion;
import torres.jimmy.bl.entities.Usuario;

import java.util.ArrayList;



public class Datos {


    private ArrayList<Cita> citas;
    private ArrayList<Mascota> mascotas;
    private ArrayList<Reservacion> reservaciones;
    private ArrayList<Usuario> usuarios;

    /**
     * Registra en la estructura una cita.
     * @param c
     */
    public void registrarCita(Cita c){
        if(citas == null){
            citas = new ArrayList<>();
        }
        citas.add(c);
    }
    public ArrayList<Cita> getCitas() { return citas; }

    /**
     * Registra en la estructura una mascota.
     * @param m
     */
    public void registrarMascota(Mascota m){
        if(mascotas == null){
            mascotas = new ArrayList<>();
        }
        mascotas.add(m);
    }
    public ArrayList<Mascota> getMascotas() { return mascotas; }
    public void setMascotas(ArrayList<Mascota> mascotas) { this.mascotas = mascotas; }


    /**
     * Registra en la estructura una reservacion.
     * @param r
     */
    public void registrarReservacion(Reservacion r){
        if(reservaciones == null){
            reservaciones = new ArrayList<>();
        }
        reservaciones.add(r);
    }
    public ArrayList<Reservacion> getReservaciones() { return reservaciones; }

    /**
     * Registra en la estructura un usario.
     * @param u
     */
    public void registrarUsuario(Usuario u){
        if(usuarios == null){
            usuarios = new ArrayList<>();
        }
        usuarios.add(u);
    }
    public ArrayList<Usuario> getUsuarios() { return usuarios; }




}
