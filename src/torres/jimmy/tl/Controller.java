package torres.jimmy.tl;

import torres.jimmy.bl.Gestor;
import torres.jimmy.bl.entities.Cita;
import torres.jimmy.bl.entities.Mascota;
import torres.jimmy.bl.entities.Reservacion;
import torres.jimmy.bl.entities.Usuario;
import torres.jimmy.ui.UI;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.util.ArrayList;

public class Controller {

    private UI ui = new UI();
    private Gestor localGestor = new Gestor();

    DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("HH:mm");


    public Controller(){

    }

    public void iniciar() throws IOException {
        int opcion = -1;
        do {
            ui.mostrarMenu();
            opcion = ui.leerOpcion();
            procesarOpcion(opcion);
        } while (opcion != 0);
    }

    public void procesarOpcion(int pOpcion) throws java.io.IOException
    {
        switch (pOpcion)
        {
            case 1:
                registrarMascota();
                break;
            case 2:
                listarMascotas();
                break;
            case 3:
                registrarCita();
                break;
            case 4:
                listarCitas();
                break;
            case 5:
                registrarReservacion();
                break;
            case 6:
                listarReservaciones();
                break;
            case 7:
                registrarUsuario();;
                break;
            case 8:
                listarUsuarios();
                break;
            case 0:
                ui.imprimirMensaje("Fin del programa");
                break;
            default:
                ui.imprimirMensaje("Opción inválidad");
        }
    }

    public void registrarCita() throws java.io.IOException{
        String nombreMascota;
        LocalDate fechaCita;
        LocalTime horaCita;
        String observaciones;

        ui.imprimirMensaje("Digite el nombre de la mascota:");
        nombreMascota = ui.leerTexto();
        ui.imprimirMensaje("Digite la fecha de la cita (formato: 31/12/2020):");
        fechaCita = LocalDate.parse(ui.leerTexto(), dateFormat);
        ui.imprimirMensaje("Digite la hora de la cita (formato: 23:59):");
        horaCita = LocalTime.parse(ui.leerTexto(), timeFormat);
        ui.imprimirMensaje("Digite las observaciones:");
        observaciones = ui.leerTexto();

        localGestor.registrarCita(nombreMascota, fechaCita, horaCita, observaciones);
    }

    public void registrarReservacion() throws java.io.IOException {

        String nombreMascota;
        LocalDate fechaEntrada;
        LocalDate fechaSalida;


        ui.imprimirMensaje("Digite el nombre de la mascota: "); nombreMascota = ui.leerTexto();
        ui.imprimirMensaje("Digite la fecha de entrada (formato: 31/12/2020): "); fechaEntrada = LocalDate.parse(ui.leerTexto(), dateFormat);
        ui.imprimirMensaje("Digite la fecha de salida (formato: 31/12/2020): "); fechaSalida = LocalDate.parse(ui.leerTexto(), dateFormat);

        localGestor.registrarReservacion(nombreMascota, fechaEntrada, fechaSalida);
    }

    public void registrarUsuario() throws java.io.IOException {

        String nombreUsuario;
        String cedula;
        String telefono;
        String direccion;
        int estado; //activo/inactivo
        int rol; // doctor/secretaria

        ui.imprimirMensaje("Digite el nombre del usuario: "); nombreUsuario = ui.leerTexto();


        boolean cedulaDuplicada;
        do {
            ui.imprimirMensaje("Digite la cedula del usuario: ");
            cedula = ui.leerTexto();
            if (this.numeroCedulaDuplicada(cedula) == true) {
                ui.imprimirMensaje("Esta cedula esta duplicada. Intentar de nuevo...");
                cedulaDuplicada = true;
            } else{
                cedulaDuplicada = false;
            }
        }while( cedulaDuplicada == true );

        ui.imprimirMensaje("Digite el numero de telefono: "); telefono = ui.leerTexto();
        ui.imprimirMensaje("Digite la direccion: "); direccion = ui.leerTexto();

        boolean estadoValido;
        do {
            estadoValido = true;
        ui.imprimirMensaje("Digite el estado (1 si es activo, 2 si es inactivo): ");
        estado = Integer.parseInt(ui.leerTexto());
            if( !((estado == 1) || (estado == 2) )){
                estadoValido = false;
                System.out.println("El numero que ingreso no es 1 ni 2. Intente de nuevo...");
            }
        }while(!estadoValido);

        boolean rolValido;
        do {
            rolValido = true;
        ui.imprimirMensaje("Digite el rol (1 si es doctor, 2 si es secretaria): ");
        rol = Integer.parseInt(ui.leerTexto());
            if( !((rol == 1) || (rol == 2) )){
                rolValido = false;
                System.out.println("El numero que ingreso no es 1 ni 2. Intente de nuevo...");
            }
        }while(!rolValido);

        localGestor.registrarUsuario(nombreUsuario, cedula, telefono, direccion, estado, rol);
    }

    public void registrarMascota() throws java.io.IOException {
        String nombre;
        String foto;
        String observaciones;
        int ranking;
        String nombreDueno;
        String cedula;
        String telefono;
        String direccion;

        boolean nombreDuplicado;
        do {
            ui.imprimirMensaje("Digite el nombre de la mascota: ");
            nombre = ui.leerTexto();
            if (this.nombreMascotaDuplicado(nombre) == true) {
                ui.imprimirMensaje("Este nombre esta duplicado. Intentar de nuevo...");
                nombreDuplicado = true;
            } else{
                nombreDuplicado = false;
            }
        }while( nombreDuplicado == true );

        ui.imprimirMensaje("Ingrese la foto de la mascota: "); foto = ui.leerTexto();
        ui.imprimirMensaje("Digite los cuidados especiales de la mascota: "); observaciones = ui.leerTexto();

        boolean rankingValido;
        do {
            rankingValido = true;
            ui.imprimirMensaje("Digite el ranking (de 1 a 3): ");
            ranking = Integer.parseInt(ui.leerTexto());

            if( !((ranking == 1) || (ranking == 2) || (ranking == 3))){
                rankingValido = false;
                System.out.println("No es un numero entre 1 y 3. Intente de nuevo...");
            }

        }while(!rankingValido);

        ui.imprimirMensaje("Digite el nombre del dueño: "); nombreDueno = ui.leerTexto();
        ui.imprimirMensaje("Digite la cedula del dueño: "); cedula = ui.leerTexto();
        ui.imprimirMensaje("Digite el telefono del dueño "); telefono = ui.leerTexto();
        ui.imprimirMensaje("Digite la dirección del dueño "); direccion = ui.leerTexto();

        localGestor.registrarMascota(nombre, foto, observaciones, ranking, nombreDueno, cedula, telefono, direccion);

    }

    public void listarCitas(){
        ui.imprimirMensaje("Lista de las citas: ");
        ArrayList<Cita> citas = localGestor.listarCitas();
        for(Cita tmpCita : citas){
            ui.imprimirMensaje(tmpCita.toString());
        }
    }

    public void listarMascotas(){
        ui.imprimirMensaje("Lista de mascotas: ");
        ArrayList<Mascota> mascotas = localGestor.listarMascotas();
        for(Mascota tmpMascota : mascotas){
            ui.imprimirMensaje(tmpMascota.toString());
        }
    }

    public void listarReservaciones(){
        ui.imprimirMensaje("Lista de reservaciones: ");
        ArrayList<Reservacion> reservaciones = localGestor.listarReservaciones();
        for(Reservacion tmpReservacion : reservaciones){
            ui.imprimirMensaje(tmpReservacion.toString());
        }
    }

    public void listarUsuarios(){
        ui.imprimirMensaje("Lista de reservaciones: ");
        ArrayList<Usuario> usuarios = localGestor.listarUsuarios();
        for(Usuario tmpUsuario : usuarios){
            ui.imprimirMensaje(tmpUsuario.toString());
        }
    }

    public boolean nombreMascotaDuplicado(String nombreMascota){
        boolean duplicado = false;
        ArrayList<Mascota> mascotas = localGestor.listarMascotas();
        if(mascotas != null) {
            for (Mascota tmpMascota : mascotas) {
                if (nombreMascota.contains(tmpMascota.getNombreMascota())) {
                    duplicado = true;
                    break;
                }
            }
        }
        return duplicado;
    }


    public boolean numeroCedulaDuplicada(String numeroCedula){
        boolean duplicado = false;
        ArrayList<Usuario> usuarios = localGestor.listarUsuarios();
        if(usuarios != null) {
            for (Usuario tmpUsuario : usuarios) {
                if (numeroCedula.contains(tmpUsuario.getCedula())) {
                    duplicado = true;
                    break;
                }
            }
        }
        return duplicado;
    }





}
