package torres.jimmy.ui;

import java.io.*;

public class UI {

    private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private PrintStream out = System.out;

    public void mostrarMenu() throws java.io.IOException
    {
        out.println("1. Registrar Mascota");
        out.println("2. Listar Mascotas");
        out.println("3. Registrar Cita");
        out.println("4. Listar Citas");
        out.println("5. Registrar Reservacion");
        out.println("6. Listar Reservaciones");
        out.println("7. Registrar Usuario");
        out.println("8. Listar Usuarios");
        out.println("0. Salir del sistema");
        out.println("Digite la opción:");
    }

    /**
     * Imprime las salidas a la pantalla
     * @param mensaje la información que debe imprimirse
     */
    public void imprimirMensaje(String mensaje) {
        out.println(mensaje);
    }

    /**
     * Leer las entradas del usuario
     * @return la información digitada por el usuario
     * @throws IOException
     */
    public String leerTexto() throws IOException{
        return in.readLine();
    }

    /**
     * Solicita y retorna la opción del menú seleccinada por el usuario
     * @return el entero con la opción seleccionada por el usuario.
     * @throws IOException
     */
    public int leerOpcion() throws IOException{
        out.println("Su opcion es: ");
        return Integer.parseInt(in.readLine());
    }

}
